/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author samu
 */
public class Circulo extends Elipse {
    private double raio;
    
    public Circulo(double raio){
        super(2*raio, 2*raio); //No circulo, o diametro equivale ao eixos menor e maior.Por isso multiplicamos por 2.
    }
    
    @Override
    public double getPerimetro(){
        
        return Math.PI*super.getEixoMaior(); // Não multiplicamos por 2, pois getMaior equivale ao diametro do circulo. 
    }
}
