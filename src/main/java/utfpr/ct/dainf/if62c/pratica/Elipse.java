/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author samu
 */
public class Elipse implements FiguraComEixos {
    private double eixoMaior;
    private double eixoMenor;
    
    
    public Elipse(double eixoMaior, double eixoMenor){
        this.eixoMaior = eixoMaior;
        this.eixoMenor = eixoMenor;
    }
    
    
    @Override
    public double getEixoMenor(){
        return eixoMenor;
    }
    
    @Override
    public double getEixoMaior(){
        return eixoMaior;
    }
    
    public void setEixoMenor(double eixoMenor){
        this.eixoMenor = eixoMenor;
    }
    
        public void setEixoMaior(double eixoMaior) {
        this.eixoMaior = eixoMaior;
    }
        
    @Override
    public double getPerimetro(){
        eixoMaior = eixoMaior/2;
        eixoMenor = eixoMenor/2;
        
        return Math.PI*(3*(eixoMaior + eixoMenor) - Math.sqrt((3*eixoMaior + eixoMenor)*(eixoMaior + 3*eixoMenor)));
    }
    
    @Override
    public double getArea(){
        eixoMaior = eixoMaior/2;
        eixoMenor = eixoMenor/2;
        
        return Math.PI*eixoMaior*eixoMenor;
    }

}
