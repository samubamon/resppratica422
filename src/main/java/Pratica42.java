
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica42 {
    public static void main(String[] args) {
        Elipse elipse = new Elipse(4.0,2.0);
        Circulo circulo = new Circulo(3.0);
        
        System.out.println("Area Elipse: " + elipse.getArea());
        System.out.println("Perimetro Elipse " + elipse.getPerimetro());
        System.out.println("Area Circulo: " + circulo.getArea());
        System.out.println("Perimetro Circulo: " + circulo.getPerimetro());
        
   }
}